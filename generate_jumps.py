# By Maciej Wojtala

import pandas as pd

THETA_1 = "raw_SimulationLeftNumber"
THETA_2 = "raw_SimulationElapsedTime"

THETA_1_TRESHOLD = 10

def generate_jumps(df, label):
    print(df.head())
    deadline = 14400 - df[THETA_2]
    df['deadline'] = deadline
    df = df.sort_values(by=[THETA_1])
    df.to_csv('./data/' + label + '_sorts.csv')
    print(df[:40])
    buckets = generate_groups(df)
    print(buckets[:5])
    print(buckets[-5:])
    buckets_df = pd.DataFrame(buckets)
    buckets_df.to_csv('./data/' + label + '_buckets.csv')

    return 0

def are_equal_configurations(col_1, col_2):
    for name in [
        'ActWorkerCardinality_1',
        'ActWorkerCardinality_2',
        'ActWorkerCardinality_4',
        'ActWorkerCardinality_5',
        'ActWorkerCardinality_6',
        'ActWorkerCores_2_ActWorkerRam_15616',
        'ActWorkerCores_8_ActWorkerRam_32768',
        'ActWorkerCores_8_ActWorkerRam_62464',
        'ActWorkerCores_16_ActWorkerRam_30720'
    ]:
        if col_1[name] != col_2[name]:
            return False
    
    return True


def generate_groups(df):
    length = len(df)
    buckets = []
    for i in range(length):
        if i % 100 == 0:
            print(i)
        buckets.append([])

        j = i
        while j >= 0 and df[THETA_1].iloc[j] > df[THETA_1].iloc[i] - THETA_1_TRESHOLD:
            if not are_equal_configurations(df.iloc[i], df.iloc[j]):
                buckets[i].append(j)
            j -= 1
        j = i+1
        while j < length and df[THETA_1].iloc[j] < df[THETA_1].iloc[i] + THETA_1_TRESHOLD:
            if not are_equal_configurations(df.iloc[i], df.iloc[j]):
                buckets[i].append(j)
            j += 1
        if i % 100 == 0:
            print(buckets[i])

    return buckets

def main():
    df_train = pd.read_csv('./data/normalized_train.csv')
    df_valid = pd.read_csv('./data/normalized_valid.csv')
    df_test = pd.read_csv('./data/normalized_test.csv')
    generate_jumps(df_train, 'train')
    generate_jumps(df_valid, 'valid')
    generate_jumps(df_test, 'test')

if __name__ == '__main__':
    main()