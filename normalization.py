# By Maciej Wojtala

import pandas as pd
import numpy as np

CARDINALITIES = [1, 2, 4, 5, 6, 7] # cardinalities significantly represented in data

def normalize(df, df_train, label):
    print(df.head())

    one_hot_encodings = []
    normal_scalings = []
    
    for name in [
        'SimulationLeftNumber',
        'SimulationElapsedTime',
        'ETPercentile',
        'WillFinishTooSoon',
        'EstimatedRemainingTime',
        'NotFinished',
        'NotFinishedOnTime',
        'NotFinishedOnTimeContext',
        'MinimumCores',
        'MinimumCoresContext',
        'SimulationLeftNumberPrediction',
        'SimulationElapsedTimePrediction',
        'ETPercentilePrediction',
        'WillFinishTooSoonPrediction',
        'EstimatedRemainingTimePrediction',
        'NotFinishedPrediction',
        'NotFinishedOnTimePrediction',
        'NotFinishedOnTimeContextPrediction',
        'MinimumCoresPrediction',
        'MinimumCoresContextPrediction'
    ]:
        normal_scalings.append(normal_scaling(df, name, df_train))
    
    for normal in normal_scalings:
        print(normal.head())
    
    result = df[['SimulationLeftNumber', 'SimulationElapsedTime']]
    result = result.rename(columns = {'SimulationLeftNumber' : 'raw_SimulationLeftNumber',
            'SimulationElapsedTime' : 'raw_SimulationElapsedTime'})

    for i in CARDINALITIES[:-1]:
        result['ActWorkerCardinality_' + str(i)] = 1.0 * (df['ActWorkerCardinality'] == float(i))
    
    for (cores, ram) in [(2, 15616), (8, 32768), (8, 62464), (16, 30720)]:
        result['ActWorkerCores_' + str(cores) + '_ActWorkerRam_' + str(ram)] = 1.0 *(
                np.logical_and(df['ActWorkerCores'] == float(cores), df['ActWorkerRam'] == float(ram))
            )
    
    print(result)
    
    for normal in normal_scalings:
        result = pd.concat([result, normal], axis=1)
    
    print(result.head())
    result.to_csv('./data/normalized_' + label + '.csv')

def normal_scaling(df, name, df_train):
    col = df[name]
    train_col = df_train[name]
    mean = train_col.mean()
    std = train_col.std()
    return (col-mean) / std

def one_hot_encoding(df, name):
    col = df[name]
    one_hot = pd.get_dummies(col, prefix = name).iloc[:,:-1]
    return one_hot


if __name__ == '__main__':
    df_train = pd.read_csv('./data/train_all_metrics_configurations.csv').dropna()
    df_valid = pd.read_csv('./data/valid_all_metrics_configurations.csv').dropna()
    df_test = pd.read_csv('./data/test_all_metrics_configurations.csv').dropna()
    df_train = df_train[df_train['ActWorkerCardinality'].isin(CARDINALITIES)]
    df_valid = df_valid[df_valid['ActWorkerCardinality'].isin(CARDINALITIES)]
    df_test = df_test[df_test['ActWorkerCardinality'].isin(CARDINALITIES)]
    normalize(df_train, df_train, 'train')
    normalize(df_valid, df_train, 'valid')
    normalize(df_test, df_train, 'test')