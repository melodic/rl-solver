import torch.nn as nn
from Solver_Model.Model.EntityEncoder.transformer.transformer import Transformer


class EntityEncoder(nn.Module):

    def __init__(self, d_model, d_t, d_f, device):
        super().__init__()
        self.device = device
        self.linear_1 = nn.Linear(d_f, d_model).to(self.device)
        self.transformer = Transformer(d_model, d_t, d_model, 3, 2, device)
        self.relu = nn.ReLU()
        self.linear_2 = nn.Linear(d_model, d_model).to(self.device)

    def forward(self, x):
        # (batch, d_t, n)
        x = self.linear_1(x).to(self.device)
        # (batch, d_t, d_model)
        x = self.transformer(x).to(self.device)
        # (batch, d_t, d_model)
        x = self.relu(x).to(self.device)
        x = self.linear_2(x).to(self.device)
        # (batch, d_t, d_model)
        x = self.relu(x).to(self.device)
        return x
