import copy
import torch
import torch.nn as nn

from Solver_Model.Model.EntityEncoder.transformer.feed_forward import FeedForward
from Solver_Model.Model.EntityEncoder.transformer.multi_head_attention import MultiHeadAttention
from Solver_Model.Model.EntityEncoder.transformer.norm import Norm
from Solver_Model.Model.EntityEncoder.transformer.positional_encoder import PositionalEncoder


class Encoder(nn.Module):
    def __init__(self, features, max_seq_length, d_model, n, heads, device):
        super().__init__()
        self.n = n
        self.device = device
        self.embed = nn.Linear(features, d_model).to(self.device)
        self.pe = PositionalEncoder(max_seq_length, d_model, device)
        self.layers = get_clones(EncoderLayer(d_model, heads, device), n)
        self.norm = Norm(d_model, device)

    def forward(self, x):
        # x ~ (batch, max_seq_length, features)
        x = self.embed(x)
        # x ~ (batch, max_seq_length, d_model)
        #SzN: the below commented line is very risky as it depends whether embedding wouldn't zero-out certain (all 1s) vectors
        #SzN: so I created a work-around making the last dimension of a mask nonexistent (it would fit d_model later by broadcasting)
        # mask = (self.embed(mask) - self.embed.bias != 0).type(torch.float32).to(self.device)
        #print(torch.count_nonzero(mask, (1, 2)) / 4)
        x = self.pe(x)
        for i in range(self.n):
            x = self.layers[i](x)
        x = self.norm(x)
        # x ~ (batch, max_seq_length, d_model)
        return x


class EncoderLayer(nn.Module):
    def __init__(self, d_model, heads, device, dropout=0.1):
        super().__init__()
        self.device = device
        self.norm_1 = Norm(d_model, device)
        self.norm_2 = Norm(d_model, device)
        self.attn = MultiHeadAttention(heads, d_model, device)
        self.ff = FeedForward(d_model, device)
        self.dropout_1 = nn.Dropout(dropout)
        self.dropout_2 = nn.Dropout(dropout)

    def forward(self, x):
        # x ~ (batch, max_seq_length, d_model)
        x = self.norm_1(x + self.dropout_1(self.attn(x, x, x)).to(self.device))
        x = self.norm_2(x + self.dropout_2(self.ff(x)).to(self.device))
        # x ~ (batch, max_seq_length, d_model)
        return x


def get_clones(module, n):
    return nn.ModuleList([copy.deepcopy(module) for _ in range(n)])
