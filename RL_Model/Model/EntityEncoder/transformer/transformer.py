import math
import torch.nn as nn

from Solver_Model.Model.EntityEncoder.transformer.encoder import Encoder
from Solver_Model.Model.EntityEncoder.transformer.padding_and_mask import padding_and_mask


class Transformer(nn.Module):
    def __init__(self, features, max_seq_length, d_model, n, heads, device):
        super().__init__()
        self.n = n
        self.heads = heads
        self.d_model = d_model  #SzN: we need some space to accomodate for positional encoding
        if features > self.d_model:  #SzN: adjust it some more making sure it stays divisible by #heads
            self.d_model = int(features + math.fabs(heads - features) % heads)
        self.max_seq_length = max_seq_length
        self.device = device
        self.encoder = Encoder(features, max_seq_length, self.d_model, n, heads, device)

    def forward(self, x):
        x = self.encoder(x)
        # x ~ (batch, max_seq_length, d_model)
        return x
