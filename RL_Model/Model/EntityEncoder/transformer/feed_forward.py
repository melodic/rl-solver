import torch.nn as nn
import torch.nn.functional as f


class FeedForward(nn.Module):
    def __init__(self, d_model, device):
        super().__init__()
        self.device = device
        self.linear_1 = nn.Linear(d_model, 4 * d_model).to(device)
        self.linear_2 = nn.Linear(4 * d_model, d_model).to(device)

    def forward(self, x):
        # x ~ (batch, max_seq_length, d_model)
        x = f.relu(self.linear_1(x))
        # x ~ (batch, max_seq_length, 4 * d_model)
        x = self.linear_2(x)
        # x ~ (batch, max_seq_length, d_model)
        return x
