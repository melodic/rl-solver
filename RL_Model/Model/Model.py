import torch.nn as nn
import torch.nn.functional as f
from RL_Model.Model.EntityEncoder.entity_encoder import EntityEncoder
from RL_Model.Model.EntityEncoder.transformer.norm import Norm

from RL_Model.Model.winlossBaseline.winloss_baseline import WinLossBaseline
import math


class Model(nn.Module):

    def __init__(self, d_t, d_f, d_v, d_l, d_model=64, device='cuda:0'):
        super().__init__()
        self.device = device
        self.d_t = d_t
        self.d_model = d_model
        self.entity_encoder = EntityEncoder(d_model, d_t, d_f, device)
        self.out1 = nn.Linear(d_model * d_t, 4 * d_model * d_t).to(device)
        self.out2 = nn.Linear(4 * d_model * d_t, math.floor(math.sqrt(4 * d_model * d_t * d_l))).to(device)
        self.out3 = nn.Linear(math.floor(math.sqrt(4 * d_model * d_t * d_l)), d_l).to(device)
        #if self.training:
        self.winloss_baseline = WinLossBaseline(d_model, d_t, d_v, d_l, device)
        self.softmax = nn.Softmax(dim = -1)

    def forward(self, x):
        # (batch, d_t, d_f)
        e_output = self.entity_encoder(x).to(self.device)
        # (batch, d_t, d_model)
        out = self.out1(e_output.view(-1, self.d_t * self.d_model))
        # (batch, 4 * d_model * d_t)
        out = self.out2(out)
        # (batch, math.floor(math.sqrt(4 * d_model * d_t * d_l)))
        policy = self.out3(out)
        policy_logits = self.softmax(policy)
        # (batch, d_l)
        #if self.training:
        baseline = self.winloss_baseline(policy, e_output).to(self.device)
        #print("model baseline")
        #print(baseline)
        # (batch, d_v)
        return policy_logits, baseline
        #return policy_logits
