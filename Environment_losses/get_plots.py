import pandas as pd
import matplotlib
import matplotlib.pyplot as plt
import numpy as np

def main():
    generate_plot('train_losses.csv', 'valid_losses.csv', 'MSE_losses.png', 'MSE loss')
    generate_plot('train_metrics.csv', 'valid_metrics.csv', 'MAE_metrics.png', 'MAE metric')

def generate_plot(train_name, valid_name, png_name, metric_name):
    train_df = pd.read_csv(train_name).drop(columns = ['Unnamed: 0']).to_numpy()
    valid_df = pd.read_csv(valid_name).drop(columns = ['Unnamed: 0']).to_numpy()
    size = train_df.shape[0]
    print(train_df)
    plt.plot(range(size-1), train_df[:-1], 'g-')
    plt.plot(range(size-1), valid_df[1:], 'b-')
    plt.legend(["Train " + metric_name, "Validation " + metric_name])
    plt.xlabel("Training epoch")
    plt.ylabel("Value")
    plt.savefig(png_name)
    plt.show()    

if __name__ == '__main__':
    main()