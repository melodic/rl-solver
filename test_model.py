import torch
import numpy as np
import pandas as pd
from reinforcement_src.get_environment_model import get_environment_model

def test_model():
    labels = torch.load('./data/test_labels.pt')
    inputs = torch.load('./data/test_inputs.pt')
    inputs = inputs.reshape((inputs.shape[0], 1, inputs.shape[1]))
    print(inputs.shape)
    print(inputs)
    print(labels.shape)
    print(labels)
    MSEloss = torch.nn.MSELoss()
    MAEloss = torch.nn.L1Loss()
    test_losses = np.array([])
    test_metrics = np.array([])
    model = get_environment_model()
    batch = 64

    model.eval()
    for i in range(0, len(inputs), batch):
        total_loss = 0
        total_metric = 0
        output = model(inputs[i : i + batch])
        label = labels[i : i + batch]
        loss = MSEloss(output, label)
        metric = MAEloss(output, label)
        total_loss += loss.detach()
        total_metric += metric.detach()
        test_losses = np.concatenate((test_losses, np.array([total_loss.cpu()])))
        test_metrics = np.concatenate((test_metrics, np.array([total_metric.cpu()])))
    mean_loss = test_losses.mean()
    mean_metric = test_metrics.mean()
    print("Average training loss: ")
    print(mean_loss, flush = True)
    print("Average training MAE: ")
    print(mean_metric, flush = True)
    return mean_loss, mean_metric

if __name__ == '__main__':
    test_model()