# By Maciej Wojtala

import torch
from Solver_Model.Model.Model import Model

def get_models(d_t, d_f, d_v, d_l, d_model, device):
    models = torch.nn.ModuleList([Model(d_t, d_f, d_v, d_l, d_model, device).to(device)])
    for m in models:
        for param in m.parameters():
            if param.dim() > 1:
                torch.nn.init.xavier_uniform_(param)
            else:
                torch.nn.init.normal_(param)
    
    return models