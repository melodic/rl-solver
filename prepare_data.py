# By Maciej Wojtala

import pandas as pd
import numpy as np
import torch
import os

TRAIN_SIZE = 0.7
VALID_SIZE = 0.2
MAX_LENGTH = 450
CARDINALITY_SCALE = 5

def split_dataset():
    np.random.seed(10)
    prediction_df = pd.read_csv(
            './data/all_metrics_configurations_predictions_merged.csv'
        ).drop(columns = ['Unnamed: 0', 'Unnamed: 0.1'])
    prediction_df = prediction_df[prediction_df.SimulationElapsedTimePrediction > 0.0]
    chosen_predictions = prediction_df[prediction_df.experimentId % 10 == 0].drop_duplicates(
            subset=['time', 'experimentId']
        )
    dir = './data'
    for exper in chosen_predictions['experimentId'].unique():
        train_filename = "train_" + str(exper) + '_experiment.csv'
        valid_filename = "valid_" + str(exper) + '_experiment.csv'
        test_filename = "test_" + str(exper) + '_experiment.csv'
        train_path = os.path.join(dir, train_filename)
        valid_path = os.path.join(dir, valid_filename)
        test_path = os.path.join(dir, test_filename)
        print("Experiment: " + str(exper))
        df = chosen_predictions[chosen_predictions['experimentId'] == exper]
        print(len(df))
        if len(df) > MAX_LENGTH:
            df = df.drop_duplicates(subset=['SimulationLeftNumber'])

        length = len(df)
        permutation = np.random.permutation(length)
        train_len = round(length*TRAIN_SIZE)
        valid_len = round(length*VALID_SIZE)

        repeats = int(MAX_LENGTH / length)
        
        if df['ActWorkerCores'].iloc[0] == 2 and len(df['ActWorkerCardinality'].unique()) > 4:
            repeats = repeats * CARDINALITY_SCALE
        print(length)
        print(repeats)

        train_experiments = permutation[:train_len]
        valid_experiments = permutation[train_len : train_len + valid_len]
        test_experiments = permutation[train_len + valid_len:]
        train_dfs = []
        valid_dfs = []
        test_dfs = []
        for _ in range(repeats):
            train_dfs.append(df.iloc[train_experiments, :])
            valid_dfs.append(df.iloc[valid_experiments, :])
            test_dfs.append(df.iloc[test_experiments, :])
        train_set = pd.concat(train_dfs)
        valid_set = pd.concat(valid_dfs)
        test_set = pd.concat(test_dfs)

        train_set.to_csv(train_path)
        valid_set.to_csv(valid_path)
        test_set.to_csv(test_path)

def merge():
    for type in ['train', 'valid', 'test']:
        dfs = []
        dir = './data'
        prediction_df = pd.read_csv(
                './data/all_metrics_configurations_predictions_merged.csv'
            ).drop(columns = ['Unnamed: 0', 'Unnamed: 0.1'])
        prediction_df = prediction_df[prediction_df.SimulationElapsedTimePrediction > 0.0]
        chosen_predictions = prediction_df[prediction_df.experimentId % 10 == 0].drop_duplicates(
                subset=['time', 'experimentId']
            )
        dir = './data'
        for exper in chosen_predictions['experimentId'].unique():
            filename = type + "_" + str(exper) + '_experiment.csv'
            path = os.path.join(dir, filename)
            print(filename)
            df = pd.read_csv(path).dropna()
            dfs.append(df)
        merged_df = pd.concat(dfs)
        print(merged_df)
        new_filename = type + "_all_metrics_configurations.csv"
        new_path = os.path.join(dir, new_filename)
        merged_df.to_csv(new_path)

def main():
    split_dataset()
    merge()
    return 0

if __name__ == '__main__':
    main()