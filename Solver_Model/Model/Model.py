import torch
import torch.nn as nn
import torch.nn.functional as f
import numpy as np
from Solver_Model.Model.EntityEncoder.entity_encoder import EntityEncoder
from Solver_Model.Model.EntityEncoder.transformer.norm import Norm

from Solver_Model.Model.winlossBaseline.winloss_baseline import WinLossBaseline
import math


class Model(nn.Module):

    def __init__(self, d_t, d_f, d_v, d_l, d_model=64, device='cuda:0'):
        super().__init__()
        self.device = device
        self.d_t = d_t
        self.d_f= d_f
        self.d_model = d_model
        self.entity_encoder = EntityEncoder(d_model, d_t, d_f, device)
        self.out1 = nn.Linear(d_model * d_t, 4 * d_model * d_t).to(device)
        self.out2 = nn.Linear(4 * d_model * d_t, math.floor(math.sqrt(4 * d_model * d_t * d_l))).to(device)
        self.out3 = nn.Linear(math.floor(math.sqrt(4 * d_model * d_t * d_l)), d_l).to(device)
        self.winloss_baseline = WinLossBaseline(d_model, d_t, d_v, d_l, device)
        self.linear_baseline = nn.Linear(d_t * d_f, d_v).to(self.device)


    def forward(self, x):
        # (batch, d_t, d_f)
        e_output = self.entity_encoder(x).to(self.device)
        # (batch, d_t, d_model)
        out = self.out1(e_output.view(-1, self.d_t * self.d_model))
        # (batch, 4 * d_model * d_t)
        out = self.out2(out)
        baseline = self.winloss_baseline(None, e_output).to(self.device)
        return baseline
