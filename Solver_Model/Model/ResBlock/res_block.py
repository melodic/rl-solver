import torch.nn as nn
import torch.nn.functional as f


class ResBlock(nn.Module):
    def __init__(self, channels, device):
        super().__init__()
        self.device = device
        self.linear_1 = nn.Linear(channels, channels).to(self.device)
        self.linear_2 = nn.Linear(channels, channels).to(self.device)

    def forward(self, x):
        x = x + self.linear_2(self.linear_1(x))
        x = f.relu(x).to(self.device)
        return x