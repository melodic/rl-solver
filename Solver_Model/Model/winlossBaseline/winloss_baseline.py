import torch.nn as nn
import numpy as np
import torch

from Solver_Model.Model.ResBlock.res_block import ResBlock


class WinLossBaseline(nn.Module):

    def __init__(self, d_model, d_t, d_v, d_l, device):
        super().__init__()
        self.device = device
        self.d_model = d_model
        self.linear_1 = nn.Linear(d_l, d_model).to(self.device)
        self.linear_2 = nn.Linear(d_t, 1).to(self.device)
        self.norm_1 = nn.LayerNorm(d_model).to(self.device)
        self.N = 4
        self.layers = nn.ModuleList([ResBlock(d_model, device) for _ in range(self.d_model)])
        self.norm_2 = nn.LayerNorm(d_model).to(self.device)
        self.relu = nn.ReLU()
        self.linear = nn.Linear(d_model, d_v).to(self.device)

    def forward(self, policy, e_output):
        c = self.linear_2(e_output.transpose(1, 2)).to(self.device).squeeze()
        x = c.view(-1, self.d_model).to(self.device)
        x = self.norm_1(x)
        for i in range(self.N):
            x = self.layers[i](x).to(self.device)
        x = self.norm_2(x).to(self.device)
        x = self.relu(x).to(self.device)
        x = self.linear(x).to(self.device)
        return x
