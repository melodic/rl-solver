import torch


def padding_and_mask(x, max_seq_length, lens, device):
    # x ~ (batch, seq_len, features)
    mask = torch.stack([
                torch.cat((torch.ones(n), torch.zeros(max_seq_length-n))) for n in lens
        ]).to(device)   #SzN: I prefer to make the last dimension == 1 by unsqueeze later
            #and to fit any dimension by broadcasting, than to make it == #features and then struggle with making it == d_model

    x = pad(x, max_seq_length, device)

    #SzN: the below commented line brings no effect whatsoever, as mask is already padded with 0-s
    # mask = pad(mask, max_seq_length, device)

    # x ~ (batch, max_seq_length, features)
    # mask ~ (batch, max_seq_length)

    return x, mask


def pad(x, max_seq_length, device):
    return torch.cat(
        (x, torch.zeros(x.shape[0], max_seq_length - x.shape[1], x.shape[2]).to(device)), dim=1
    ).to(device)
