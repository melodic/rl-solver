\documentclass{article}
\usepackage[utf8]{inputenc}

\usepackage{graphicx}
\usepackage{float}
\usepackage{mathtools}

\title{Environment Model Description}
\author{Maciej Wojtala}
\date{January 2021}

\begin{document}
\maketitle

\section{Environment Model}

The goal of the model is to learn the Markov Decision Process of the environment, i.e. to be able to predict the next state of the agent having actual agent state and chosen action (classically we also want to predict the reward, but in our problem the reward is deterministic, so we know it if we know the new state).

Training such a model is done by simple supervised learning.

\paragraph{Data Structure}
In the dataset we use the following information:

\begin{itemize}
    \item Metric data - we use the following metrics: SimulationLeftNumber, SimulationElapsedTime, WillFinishTooSoon, EstimatedRemainingTime, ETPercentile, NotFinished, NotFinishedOnTime, NotFinishedOnTimeContext, MinimumCores.
    
    We do not use the MinumCoresContext metric, as it is too similar to MinimumCores.
    \item Prediction data - for each metric we have a respective prediction. We compare fitting of models with and without the prediction data. Note that prediction data are additional, the most important is estimating metrics, because they determine the reward.
    \item Configuration data - we encode the configuration by the following parameters: ActWorkerCardinality, ActWorkerCores, ActWorkerRam.
\end{itemize}

The dataset consists of bunch of time series, each describes values of metrics and predictions in given configuration.
There are experiments for ActWorkerCardinality equal to $1$, $2$, $4$ and changing in the range between $1$ and $10$.
For each value of ActWorkerCardinality there are experiments for following values of pairs $(\text{ActWorkerCores}, \: \text{ActWorkerRam})$: $(2, \: 15616)$, $(8, \: 32768)$, $(8, \: 62464)$, $(16, \: 30720)$, $(16, \: 65536)$.

Note that apart from experiments with changes of ActWorkerCardinality, the configuration is constant during the experiment, which is a problem, because we want to learn the behaviour of metrics after changing the configuration. Moreover, for experiments with changes of ActWorkerCardinality only for $(\text{ActWorkerCores}, \: \text{ActWorkerRam}) = (2, \: 15616)$ we obtain a diverse set of ActWorkerCardinality values, and even here some values did not appear or appered very rarely. Thus, we used for training only values of ActWorkerCardinality from the set $\{1,\: 2, \: 4, \: 5, \: 6, \: 7\}$. For the reinforcement learning part we plan using only $\text{ActWorkerCardinality} = 1, \: 2, \: 4$, because other values may strongly mislead the model, as they are underrepresented in the training dataset.

\paragraph{Data transformation}
The time series from experiments differ in length (as for some configurations of the application simulation ends faster than for others). It makes the dataset unbalanced, so we balance it using oversampling (we add some number of copies of shorter experiments to the dataset) and undersampling (we cut out some points from longer time series).

We split the data on training set, validation set and test set using random split on every experiment and gluing them together. We make sure that every added copy of given time series point ends up in the same set. Even tough neighbouring series points are quite similar and usually end up in different set. To counteract it we could have split the dataset by consistent subsequences, however it would have created a possibility that some highly important consistent subsequent of time series was not represented in the training set, so we did not follow that approach.

Then we normalize each input using gaussian normalization based on the training set - for every metric and prediction we substract its mean in the training set and divide by its standard deviation in the training set.

The configuration columns are transformed using one-hot encoding - one for ActWorkerCardinality and one for pairs (\text{ActWorkerCores}, \: \text{ActWorkerRam}).

\paragraph{State and action formalization}
Formalization of an action is quite clear - we want to encode changing configuration from one triple
\[(\text{ActWorkerCardinality}, \: \text{ActWorkerCores}, \: \text{ActWorkerRam})\] 
to another, so we simply glue these triples obtaining a sextuple

$(\text{ActWorkerCardinality}_\text{old}, \: \text{ActWorkerCores}_\text{old}, \: \text{ActWorkerRam}_\text{old}, \\ \: \text{ActWorkerCardinality}_\text{new}, \: \text{ActWorkerCores}_\text{new}, \: \text{ActWorkerRam}_\text{new})$.

States are described by the rest of informations, i.e. the metrics and predictions (if we use them).

Our goal is to predict the new state having an old state and a taken action. The triple $(\text{state}_\text{old}, \: \text{action}, \: \text{state}_\text{new})$ is called a $\textbf{transition}$.

\paragraph{Training transition estimation}
Since we do not have transitions in our dataset, we need to estimate them somehow. We rely on an assumption that we can move from a time series point $p_1$ to a time series point $p_2$ using action $a$ if they have the same value of SimulationLeftNumber and ActWorkerCardinality, ActWorkerCores, ActWorkerRam from $p_1$ and $p_2$ match the taken action $a$ (we identify a state with the corresponding time series point). In practice we set a threshold and say that we cane move from $p_1$ to $p_2$ if the absolute difference in SimulationLeftNumber is lower than the threshold. We used threshold equal to $10$ (on raw values - before normalization).

We assume also that transition is evaluated immediately, so we know the new value of SimulationElapsedTime (which is simply a timer) - it is equal to the old value. Thus we do not need to predict values of SimulationLeftNumber and SimulationElapsedTime, as we now their output values, we predict remaining metrics. In the case of predicting predictions as well, we also do not predict predictions for SimulationLeftNumber and SimulationElapsedTime.

\paragraph{Neural network}

The neural network is based on the Transformer encoder, details are described in a separate document.

\paragraph{Results}

We experimented with predicting metrics from metrics, metrics and predictions from metrics and predictions and metrics from metrics and predictions.

Predicting metrics and predictions gave poor results. The reason might be the fact that predictions introduce quite much noise (as it is noisy estimation of metrics) which affects negatively on training. The average MSE (Mean Square Error) loss achieved around 0.043 on the training set and around 0.19 on the validation set (all losses on normalized dataset, not that normalization is applied in such a manner that the best constant estimation gives the MSE equal to $1$).

Predicting metrics achieved much better results. Loss values were very similar with and without predictions in the input and as we want to have the same information in input and output (to create transitions between states) we got rid of predictions in the input.

For predicting metrics from metrics we achieved the MSE around 0.022 on the training set and around 0.047 on the validation set. We also investigated as an additional metric the value of the MAE (Mean Absolute Error). We achieved the MAE around 0.062 on the training set and around 0.086 on the validation set.

Training process for all settings was quite similar - during first few epochs (one epoch was an iteration over whole dataset) the loss had been improving significantly and than the improvements tended to become very slow. It might be caused by the fact that dataset were quite large, so during one epoch a lot of improvement were than and after few epochs the model was fitted.

\paragraph{Plots}

\begin{figure}[h]
\includegraphics[scale=0.95]{images/MSE_losses.png}

\caption{The plot shows the value of the MSE loss during training on the training set and on the validation set.}
\end{figure}

\begin{figure}[h]
\includegraphics[scale=0.95]{images/MAE_metrics.png}

\caption{The plot shows the value of the MAE metric during training on the training set and on the validation set.}
\end{figure}



\end{document}
