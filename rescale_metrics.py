import pandas as pd

CARDINALITIES = [1, 2, 4, 5, 6, 7] # cardinalities significantly represented in data


def rescale_metrics():
    df_train = pd.read_csv('./data/train_all_metrics_configurations.csv').dropna()
    df_train = df_train[df_train['ActWorkerCardinality'].isin(CARDINALITIES)]
    dict_mean = {}
    dict_std = {}
    for name in [
        'SimulationLeftNumber',
        'SimulationElapsedTime',
        'ETPercentile',
        'WillFinishTooSoon',
        'EstimatedRemainingTime',
        'NotFinished',
        'NotFinishedOnTime',
        'NotFinishedOnTimeContext',
        'MinimumCores',
        'MinimumCoresContext',
        'SimulationLeftNumberPrediction',
        'SimulationElapsedTimePrediction',
        'ETPercentilePrediction',
        'WillFinishTooSoonPrediction',
        'EstimatedRemainingTimePrediction',
        'NotFinishedPrediction',
        'NotFinishedOnTimePrediction',
        'NotFinishedOnTimeContextPrediction',
        'MinimumCoresPrediction',
        'MinimumCoresContextPrediction'
    ]:
        mean, std = mean_std(name, df_train)
        dict_mean[name] = [mean]
        dict_std[name] = [std]
    
    print(dict_mean)
    print(dict_std)
    
    dict_mean_df = pd.DataFrame.from_dict(dict_mean)
    dict_std_df = pd.DataFrame.from_dict(dict_std)
    dict_mean_df.to_csv('./data/train_means.csv')
    dict_std_df.to_csv('./data/train_stds.csv')

def mean_std(name, df_train):
    train_col = df_train[name]
    mean = train_col.mean()
    std = train_col.std()
    return mean, std

if __name__ == '__main__':
    rescale_metrics()