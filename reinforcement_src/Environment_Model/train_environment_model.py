# By Maciej Wojtala

import torch
import pandas as pd
import numpy as np

from get_models import get_models

class Trainer():
    def __init__(self):
        self.device = torch.device(torch.cuda.current_device()  if torch.cuda.is_available() else 'cpu')
        self.train_df = pd.read_csv("./data/train_sorts.csv")
        self.valid_df = pd.read_csv("./data/valid_sorts.csv")
        self.test_df = pd.read_csv("./data/test_sorts.csv")
        self.train_buckets = pd.read_csv("./data/train_buckets.csv").drop(columns = ['Unnamed: 0']).values
        self.valid_buckets = pd.read_csv("./data/valid_buckets.csv").drop(columns = ['Unnamed: 0']).values
        self.test_buckets = pd.read_csv("./data/test_buckets.csv").drop(columns = ['Unnamed: 0']).values

        self.rand_generator = torch.Generator()
        self.seed = 23
        self.rand_generator.manual_seed(self.seed)

        df = self.train_df

        self.state_index = [
            'SimulationLeftNumber' == df.columns[i]
            or 'SimulationElapsedTime' == df.columns[i]
            or 'ETPercentile' == df.columns[i]
            or 'WillFinishTooSoon' == df.columns[i]
            or 'EstimatedRemainingTime' == df.columns[i]
            or 'NotFinished' == df.columns[i]
            or 'NotFinishedOnTime' == df.columns[i]
            or 'NotFinishedOnTimeContext' == df.columns[i]
            or 'MinimumCores' == df.columns[i]
            for i in range(len(df.columns))
        ]

        self.state_2_index = [
            'ETPercentile' == df.columns[i]
            or 'WillFinishTooSoon' == df.columns[i]
            or 'EstimatedRemainingTime' == df.columns[i]
            or 'NotFinished' == df.columns[i]
            or 'NotFinishedOnTime' == df.columns[i]
            or 'NotFinishedOnTimeContext' == df.columns[i]
            or 'MinimumCores' == df.columns[i]
            for i in range(len(df.columns))
        ]

        self.action_index = [
            'ActWorkerCardinality_1' == df.columns[i]
            or 'ActWorkerCardinality_2' == df.columns[i]
            or 'ActWorkerCardinality_4' == df.columns[i]
            or 'ActWorkerCardinality_5' == df.columns[i]
            or 'ActWorkerCardinality_6' == df.columns[i]
            or 'ActWorkerCores_2_ActWorkerRam_15616' == df.columns[i]
            or 'ActWorkerCores_8_ActWorkerRam_32768' == df.columns[i]
            or 'ActWorkerCores_8_ActWorkerRam_62464' == df.columns[i]
            or 'ActWorkerCores_16_ActWorkerRam_30720' == df.columns[i]
            for i in range(len(df.columns))
        ]

        self.state_len = np.array(self.state_index).sum()
        self.state_2_len = np.array(self.state_2_index).sum()
        self.action_len = np.array(self.action_index).sum()
        self.batch = 64
        self.epochs = 400
        self.d_t = 1
        self.d_f = self.state_len + 2 * self.action_len
        self.d_v = self.state_2_len
        self.d_l = 1
        self.d_model = 64
        self.lr = 0.00002
        self.models, self.optims = self.create_model(self.lr)
        
    def init_inputs_and_labels(self):
        self.train_inputs = torch.load('./data/train_inputs.pt')
        self.train_labels = torch.load('./data/train_labels.pt')
        self.train_inputs = self.train_inputs.reshape((self.train_inputs.shape[0], 1, self.train_inputs.shape[1]))
        self.valid_inputs = torch.load('./data/valid_inputs.pt')
        self.valid_labels = torch.load('./data/valid_labels.pt')
        self.valid_inputs = self.valid_inputs.reshape((self.valid_inputs.shape[0], 1, self.valid_inputs.shape[1]))
        self.test_inputs = torch.load('./data/test_inputs.pt')
        self.test_labels = torch.load('./data/test_labels.pt')
        self.test_inputs = self.test_inputs.reshape((self.test_inputs.shape[0], 1, self.test_inputs.shape[1]))


    def prepare_train_data(self, df, type, buckets):
        device = self.device
        print(df)
        print(df.columns)
        state_index = self.state_index
        state_2_index = self.state_2_index
        action_index = self.action_index
        print(df.iloc[:, state_index].columns)
        print(df.iloc[:, action_index].columns)
        inputs = []
        labels = []
        idx = 0
        print(buckets[:5])
        for i in range(len(df)):
            print(i, flush = True)
            bucket = buckets[i][~np.isnan(buckets[i])]
            state_1 = torch.Tensor(df.iloc[i, state_index]).to(device)
            action_1 = torch.Tensor(df.iloc[i, action_index]).to(device)
            for j in range(len(bucket)):
                action_2 = torch.Tensor(df.iloc[j, action_index]).to(device)
                state_2 = torch.Tensor(df.iloc[j, state_2_index]).to(device)
                input = torch.cat([state_1, action_1, action_2]).to(device)
                inputs.append(input)
                labels.append(state_2)
        torch_inputs = torch.stack(inputs).to(device)
        torch_labels = torch.stack(labels).to(device)
        print(torch_inputs[:5])
        print(torch_labels[:5])
        torch.save(torch_inputs, './data/' + type + '_inputs.pt')
        torch.save(torch_labels, './data/' + type + '_labels.pt')
        loaded_inputs = torch.load('./data/' + type + '_inputs.pt')
        loaded_labels = torch.load('./data/' + type + '_labels.pt')
        print("loaded")
        print(loaded_inputs[:5])
        print(loaded_labels[:5])
        print(loaded_inputs.shape)
        print(loaded_labels.shape)

    def create_model(self, lr):
        models = get_models(self.d_t, self.d_f, self.d_v, self.d_l, self.d_model, self.device)
        optims = [torch.optim.Adam(model.parameters(), lr) for model in models]
        return models, optims
    
    def train(self, model, optim):
        perm = torch.randperm(len(self.train_inputs), generator = self.rand_generator)
        print(perm.shape)
        print(perm)
        inputs = self.train_inputs[perm]
        labels = self.train_labels[perm]
        print(inputs.shape)
        print(inputs)
        print(labels.shape)
        print(labels)
        MSEloss = torch.nn.MSELoss()
        MAEloss = torch.nn.L1Loss()
        train_losses = np.array([])
        mean_losses = np.array([])
        min_validation_metric = -1
        save_mean_losses = []
        save_mean_metrics = []
        save_mean_valid_losses = []
        save_mean_valid_metrics = []


        for epoch in range(self.epochs):
            train_losses = np.array([])
            train_metrics = np.array([])
            validation_loss, validation_metric = self.validate(model)
            save_mean_valid_losses.append(validation_loss)
            save_mean_valid_metrics.append(validation_metric)
            if min_validation_metric == -1 or validation_metric < min_validation_metric:
                torch.save(model.state_dict(), './Saved_Environment_Models/Environment_Model.pt')
                min_validation_metric = validation_metric
            save_mean_valid_losses_df = pd.DataFrame(save_mean_valid_losses)
            save_mean_valid_metrics_df = pd.DataFrame(save_mean_valid_metrics)
            save_mean_valid_losses_df.to_csv('./Environment_losses/valid_losses.csv')
            save_mean_valid_metrics_df.to_csv('./Environment_losses/valid_metrics.csv')

            model.train()
            print("Epoch: ")
            print(epoch)
            for i in range(0, len(inputs), self.batch):
                total_loss = 0
                total_metric = 0
                optim.zero_grad()
                output = model(inputs[i : i + self.batch])
                label = labels[i : i + self.batch]

                loss = MSEloss(output, label)
                metric = MAEloss(output, label)
                loss.backward()
                optim.step()

                total_loss += loss.detach()
                total_metric += metric.detach()
                train_losses = np.concatenate((train_losses, np.array([total_loss.cpu()])))
                train_metrics = np.concatenate((train_metrics, np.array([total_metric.cpu()])))
            mean_loss = train_losses.mean()
            save_mean_losses.append(mean_loss)
            mean_metric = train_metrics.mean()
            save_mean_metrics.append(mean_metric)
            mean_losses = np.concatenate((mean_losses, np.array([mean_loss])))
            print("Average loss: ")
            print(mean_loss, flush = True)
            print("Average MAE: ")
            print(mean_metric, flush = True)
            save_mean_losses_df = pd.DataFrame(save_mean_losses)
            save_mean_metrics_df = pd.DataFrame(save_mean_metrics)
            save_mean_losses_df.to_csv('./Environment_losses/train_losses.csv')
            save_mean_metrics_df.to_csv('./Environment_losses/train_metrics.csv')

        validation_loss, validation_metric = self.validate(model)
        if min_validation_metric == -1 or validation_metric < min_validation_metric:
            torch.save(model.state_dict(), './Saved_Environment_Models/Environment_Model.pt')
            min_validation_metric = validation_metric
        save_mean_losses_df = pd.DataFrame(save_mean_losses)
        save_mean_metrics_df = pd.DataFrame(save_mean_metrics)
        save_mean_losses_df.to_csv('./Environment_losses/train_losses.csv')
        save_mean_metrics_df.to_csv('./Environment_losses/train_metrics.csv')
        save_mean_valid_losses_df = pd.DataFrame(save_mean_valid_losses)
        save_mean_valid_metrics_df = pd.DataFrame(save_mean_valid_metrics)
        save_mean_valid_losses_df.to_csv('./Environment_losses/valid_losses.csv')
        save_mean_valid_metrics_df.to_csv('./Environment_losses/valid_metrics.csv')

    def validate(self, model):
        print("Validation")
        inputs = self.valid_inputs
        labels = self.valid_labels
        print(inputs.shape)
        print(inputs)
        print(labels.shape)
        print(labels)
        MSEloss = torch.nn.MSELoss()
        MAEloss = torch.nn.L1Loss()
        valid_losses = np.array([])
        valid_metrics = np.array([])

        model.eval()
        for i in range(0, len(inputs), self.batch):
            total_loss = 0
            total_metric = 0
            output = model(inputs[i : i + self.batch])
            label = labels[i : i + self.batch]
            loss = MSEloss(output, label)
            metric = MAEloss(output, label)
            total_loss += loss.detach()
            total_metric += metric.detach()
            valid_losses = np.concatenate((valid_losses, np.array([total_loss.cpu()])))
            valid_metrics = np.concatenate((valid_metrics, np.array([total_metric.cpu()])))
        mean_loss = valid_losses.mean()
        mean_metric = valid_metrics.mean()
        print("Average validation loss: ")
        print(mean_loss, flush = True)
        print("Average validation MAE: ")
        print(mean_metric, flush = True)
        return mean_loss, mean_metric


def main():
    trainer = Trainer()
    trainer.prepare_train_data(trainer.test_df, 'test', trainer.test_buckets)
    trainer.prepare_train_data(trainer.valid_df, 'valid', trainer.valid_buckets)
    trainer.prepare_train_data(trainer.train_df, 'train', trainer.train_buckets)
    trainer.init_inputs_and_labels()
    for i in range(len(trainer.models)):
        trainer.train(trainer.models[i], trainer.optims[i])