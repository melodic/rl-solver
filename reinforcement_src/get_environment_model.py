import torch
from reinforcement_src.Environment_Model.train_environment_model import Trainer


def get_environment_model():
    trainer = Trainer()
    models, _ = trainer.create_model(0.1)
    model = models[0]
    path = './Saved_Environment_Models/Environment_Model.pt'
    model_state = torch.load(path)
    model.load_state_dict(model_state)

    return model