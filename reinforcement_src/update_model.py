# By Maciej Wojtala

from reinforcement_src.loss_function import loss_function
import torch

def update_model(agent, trajectories):
    print("update training")
    model = agent.get_model()

    # set training parameters
    epochs = 1
    optim = torch.optim.Adam(model.parameters(), lr=0.000001)

    model.train()

    for epoch in range(epochs):
        optim.zero_grad()

        states = torch.stack([trajectories[i].get_state() for i in range(len(trajectories))])
        input = states
        input = input.reshape((input.shape[0], 1, input.shape[1]))

        preds = [model(input[i]) for i in range(len(trajectories))]
        target_logits = torch.stack([preds[i][0] for i in range(len(preds))])
        baselines = torch.stack([preds[i][1] for i in range(len(preds))]).flatten()

        loss = torch.zeros(1).to(model.device)
        loss, loss_entropy, loss_upgo, loss_td = loss_function(trajectories, target_logits,
                baselines, model.device)
        
        print("entropy loss:")
        print(loss_entropy)
        print("actor loss:")
        print(loss_upgo)
        print("critic loss:")
        print(loss_td)

        loss.backward()
        optim.step()

        total_loss = loss.detach().cpu()

        print("Epoch: " + str(epoch))
        print("Total loss: " + str(total_loss) + '\n')
        print("Actor loss: " + str(loss_upgo) + '\n')
        print("Critic loss: " + str(loss_td) + '\n')
        print("Entropy loss: " + str(loss_entropy) + '\n')
    return total_loss.numpy(), loss_upgo.numpy(), loss_td.numpy(), loss_entropy.numpy()


