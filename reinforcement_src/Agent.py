# By Maciej Wojtala

import torch
import numpy as np
import pandas as pd
from reinforcement_src.Trajectory import Trajectory

from reinforcement_src.update_model import update_model
from reinforcement_src.Action import ACTION_LEN, Action
from reinforcement_src.State import State

UPDATE_STEP = 4
RESET_STEP = 4
VALID_STEP = 1 #4

class Agent():
    
    def __init__(self, model, environment_model, device,
                update_step = UPDATE_STEP, valid_step = VALID_STEP, reset_step = RESET_STEP):
        self.model = model
        self.environment_model = environment_model
        self.device = device
        self.total_losses = []
        self.upgo_losses = []
        self.td_losses = []
        self.entropy_losses = []
        self.update_counter = 0
        self.update_step = update_step
        self.reset_step = reset_step
        self.valid_step = valid_step
        self.trajectories = []
        self.state = State(self.device)
        self.action = Action(self.device)
        self.train_df = pd.read_csv("./data/train_sorts.csv")
        self.train_df_len = len(self.train_df)
        self.valid_df = pd.read_csv("./data/valid_sorts.csv")
        self.valid_df_len = len(self.valid_df)
        self.valid_row = 0

        self.state_index = [
            'SimulationLeftNumber' == self.train_df.columns[i]
            or 'SimulationElapsedTime' == self.train_df.columns[i]
            or 'ETPercentile' == self.train_df.columns[i]
            or 'WillFinishTooSoon' == self.train_df.columns[i]
            or 'EstimatedRemainingTime' == self.train_df.columns[i]
            or 'NotFinished' == self.train_df.columns[i]
            or 'NotFinishedOnTime' == self.train_df.columns[i]
            or 'NotFinishedOnTimeContext' == self.train_df.columns[i]
            or 'MinimumCores' == self.train_df.columns[i]
            for i in range(len(self.train_df.columns))
        ]

        self.action_index = [
            'ActWorkerCardinality_1' == self.train_df.columns[i]
            or 'ActWorkerCardinality_2' == self.train_df.columns[i]
            or 'ActWorkerCardinality_4' == self.train_df.columns[i]
            or 'ActWorkerCardinality_5' == self.train_df.columns[i]
            or 'ActWorkerCardinality_6' == self.train_df.columns[i]
            or 'ActWorkerCores_2_ActWorkerRam_15616' == self.train_df.columns[i]
            or 'ActWorkerCores_8_ActWorkerRam_32768' == self.train_df.columns[i]
            or 'ActWorkerCores_8_ActWorkerRam_62464' == self.train_df.columns[i]
            or 'ActWorkerCores_16_ActWorkerRam_30720' == self.train_df.columns[i]
            for i in range(len(self.train_df.columns))
        ]

        self.sample_state()

    def get_trajectories(self):
        return self.trajectories
    
    def get_model(self):
        return self.model
    
    def get_position(self):
        return self.position

    def get_total_losses(self):
        return np.array(self.total_losses)

    def get_upgo_losses(self):
        return np.array(self.upgo_losses)

    def get_td_losses(self):
        return np.array(self.td_losses)

    def get_entropy_losses(self):
        return np.array(self.entropy_losses)
    
    def set_total_losses(self, value):
        self.total_losses = value

    def set_upgo_losses(self, value):
        self.upgo_losses = value

    def set_td_losses(self, value):
        self.td_losses = value

    def set_entropy_losses(self, value):
        self.entropy_losses = value

    def get_total_valid_losses(self):
        return np.array(self.total_valid_losses)

    def get_upgo_valid_losses(self):
        return np.array(self.upgo_valid_losses)

    def get_td_valid_losses(self):
        return np.array(self.td_valid_losses)

    def get_entropy_valid_losses(self):
        return np.array(self.entropy_valid_losses)
    
    def set_total_valid_losses(self, value):
        self.total_valid_losses = value

    def set_upgo_valid_losses(self, value):
        self.upgo_valid_losses = value

    def set_td_valid_losses(self, value):
        self.td_valid_losses = value

    def set_entropy_valid_losses(self, value):
        self.entropy_valid_losses = value
    
    def increase_save_step(self):
        self.save_step += 1
    
    def reset_update_counter(self):
        self.update_counter = 0

    def reset_valid_row(self):
        self.valid_row = 0

    def make_step(self):
        self.model.eval()
        policy_logits, baseline = self.model(self.state.state_tensor)
        policy_logits = policy_logits.squeeze()
        print("logits")
        print(policy_logits)
        print("baseline")
        print(baseline)
        action_index = np.random.choice(range(ACTION_LEN), p = policy_logits.detach().cpu().numpy())
        print("action_index")
        print(action_index)
        self.action.set_action(action_index)
        action_tensor = self.action.action_one_hot
        is_final = torch.zeros(1).to(self.device)
        if self.state.get_SimulationLeftNumber() <= 0.0:
            is_final[0] = 1.0
        
        old_state = self.state.state_tensor.squeeze()

        environment_model_input = torch.cat([self.state.state_tensor.squeeze(), action_tensor]).to(self.device)
        environment_model_input = environment_model_input.reshape((1, environment_model_input.shape[0]))
        environment_model_output = self.environment_model(environment_model_input).squeeze()
        SimulationLeftNumber_metric = torch.tensor([self.state.SimulationLeftNumber]).to(self.device)
        SimulationElapsedTime_metric = torch.tensor([self.state.SimulationElapsedTime]).to(self.device)
        env_state_tensor = torch.cat([SimulationLeftNumber_metric, SimulationElapsedTime_metric, environment_model_output]).to(self.device)
        self.state.set_state(env_state_tensor, action_tensor)

        reward = self.calculate_reward(self.state, self.device)

        return old_state, action_tensor, action_index, reward, is_final
    
    def sample_state(self):
        row_nr = np.random.choice(range(self.train_df_len))
        print("row_nr")
        print(row_nr)
        env_state_df = self.train_df.iloc[row_nr, self.state_index]
        action_df = self.train_df.iloc[row_nr, self.action_index]
        env_state_tensor = torch.from_numpy(env_state_df.values).to(self.device)
        action_tensor = torch.from_numpy(action_df.values).to(self.device)
        self.state.set_state(env_state_tensor, action_tensor)
    
    def sample_valid_state(self):
        row_nr = self.valid_row
        self.valid_row += 1
        print("row_nr")
        print(row_nr)
        env_state_df = self.valid_df.iloc[row_nr, self.state_index]
        action_df = self.valid_df.iloc[row_nr, self.action_index]
        env_state_tensor = torch.from_numpy(env_state_df.values).to(self.device)
        action_tensor = torch.from_numpy(action_df.values).to(self.device)
        self.state.set_state(env_state_tensor, action_tensor)

    
    def calculate_reward_first_version(self, state):
        WorkerCores = state.get_cores()
        WorkerCardinality = state.get_cardinality()
        SimulationLeftNumber = state.get_SimulationLeftNumber()
        ETPercentile = state.get_ETPercentile()
        SimulationElapsedTime = state.get_SimulationElapsedTime()
        WorkerPrice = WorkerCores / 5
        first_part_exp = (SimulationLeftNumber * ETPercentile /
                (WorkerCardinality * WorkerCores) +
                SimulationElapsedTime - 3600) / 3600 * 5
        first_part = 1 / (1 + torch.exp(first_part_exp))
        second_part = 1 / (1 + WorkerCardinality * WorkerPrice)
        reward = 0.1 * first_part + 0.9 * second_part
        #print("SimulationLeftNumber")
        #print(SimulationLeftNumber)
        #print("ETPercentile")
        #print(ETPercentile)
        #print("SimulationElapsedTime")
        #print(SimulationElapsedTime)
        #print("worker cores")
        #print(WorkerCores)
        #print("worker cardinality")
        #print(WorkerCardinality)
        #print("reward first part exp")
        #print(first_part_exp)
        #print("reward first part")
        #print(first_part)
        #print("reward second part")
        #print(second_part)
        return reward

    def calculate_reward(self, state, device):
        WorkerCores = state.get_cores()
        WorkerCardinality = state.get_cardinality()
        WorkerRAM = state.get_ram()
        EstimatedRemainingTime = state.get_EstimatedRemainingTime()
        SimulationElapsedTime = state.get_SimulationElapsedTime()
        WorkerPrice = WorkerCores / 5 + WorkerRAM / 30000
        first_part_exp = (EstimatedRemainingTime +
                SimulationElapsedTime - 3600) / 3600 * 2
        first_part = 1 / (1 + torch.exp(first_part_exp))
        second_part = 1 / (1 + WorkerCardinality * WorkerPrice)
        reward = torch.zeros(1).to(device)
        reward[0] = 0.9 * first_part + 0.1 * second_part
        #print("SimulationLeftNumber")
        #print(SimulationLeftNumber)
        #print("ETPercentile")
        #print(ETPercentile)
        #print("SimulationElapsedTime")
        #print(SimulationElapsedTime)
        #print("EstimatedRemainingTime")
        #print(EstimatedRemainingTime)
        #print("worker cores")
        #print(WorkerCores)
        #print("worker RAM")
        #print(WorkerRAM)
        #print("worker cardinality")
        #print(WorkerCardinality)
        #print("reward first part exp")
        #print(first_part_exp)
        #print("reward first part")
        #print(first_part)
        #print("reward second part")
        #print(second_part)
        if EstimatedRemainingTime + SimulationElapsedTime > 2*3600:
            reward[0] = 0.0
        return reward.detach()

    
    def update(self, state, action, action_index, reward, is_final):
        print("update_counter:")
        print(self.update_counter)

        self.update_counter += 1

        trajectory = Trajectory(state, action, action_index, reward, is_final, self.device)
        self.trajectories.append(trajectory)

        if self.update_counter >= self.update_step:
            self.update_counter = 0
            self.sample_state()
            total_loss, loss_upgo, loss_td, loss_entropy = update_model(self, self.trajectories[:self.update_step])
            self.total_losses.append(total_loss)
            self.upgo_losses.append(loss_upgo)
            self.td_losses.append(loss_td)
            self.entropy_losses.append(loss_entropy)
            self.trajectories = self.trajectories[self.update_step:]
        
    def validate(self):
        print("validate_counter:")
        print(self.update_counter)

        self.update_counter += 1

        if self.update_counter >= self.valid_step:
            self.update_counter = 0
            self.sample_valid_state()
