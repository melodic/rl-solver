# By Maciej Wojtala

import torch

class Trajectory():
    def __init__(self, state, action, action_index, reward, is_final_step, device):
        self.reward = torch.zeros(1).to(device)
        self.reward[0] = reward
        self.is_final_step = torch.zeros(1).to(device)
        self.is_final_step[0] = is_final_step
        self.state = state
        self.action = action
        self.action_index = int(action_index)
    
    def get_state(self):
        return self.state
    
    def get_action(self):
        return self.action
    
    def get_action_index(self):
        return self.action_index
    
    def get_reward(self):
        return self.reward

    def get_is_final_step(self):
        return self.is_final_step
    
