import torch

CARDINALITY_LEN = 3
CORES_LEN = 5
ACTION_LEN = CARDINALITY_LEN * CORES_LEN
CARDINALITY_OFFSET = 5
ACTION_ONE_HOT_LEN = 9

class Action():
    def __init__(self, device):
        self.device = device

    def set_action(self, action_index):
        self.action_one_hot = torch.zeros(ACTION_ONE_HOT_LEN).to(self.device)
        i = int(action_index / CORES_LEN)
        j = action_index % CORES_LEN
        self.action_one_hot[i] = 1.0
        if j < CORES_LEN - 1:
            self.action_one_hot[CARDINALITY_OFFSET + j] = 1.0