import torch
import pandas as pd
from reinforcement_src.Action import ACTION_ONE_HOT_LEN

STATE_LEN = 9 + ACTION_ONE_HOT_LEN

class State():
    def __init__(self, device):
        self.device = device
        self.means_df = pd.read_csv('./data/train_means.csv')
        self.stds_df = pd.read_csv('./data/train_stds.csv')
    
    def set_state(self, state_tensor, configuration_tensor):
        self.SimulationLeftNumber = state_tensor[0].to(self.device)
        self.SimulationElapsedTime = state_tensor[1].to(self.device)
        self.ETPercentile = state_tensor[2].to(self.device)
        self.WillFinishTooSoon = state_tensor[3].to(self.device)
        self.EstimatedRemainingTime = state_tensor[4].to(self.device)
        self.NotFinished = state_tensor[5].to(self.device)
        self.NotFinishedOnTime = state_tensor[6].to(self.device)
        self.NotFinishedOnTimeContext = state_tensor[7].to(self.device)
        self.MinimumCores = state_tensor[8].to(self.device)
        self.ActWorkerCardinality_1 = configuration_tensor[0].to(self.device)
        self.ActWorkerCardinality_2 = configuration_tensor[1].to(self.device)
        self.ActWorkerCardinality_4 = configuration_tensor[2].to(self.device)
        self.ActWorkerCardinality_5 = configuration_tensor[3].to(self.device)
        self.ActWorkerCardinality_6 = configuration_tensor[4].to(self.device)
        self.ActWorkerCores_2_ActWorkerRam_15616 = configuration_tensor[5].to(self.device)
        self.ActWorkerCores_8_ActWorkerRam_32768 = configuration_tensor[6].to(self.device)
        self.ActWorkerCores_8_ActWorkerRam_62464 = configuration_tensor[7].to(self.device)
        self.ActWorkerCores_16_ActWorkerRam_30720 = configuration_tensor[8].to(self.device)

        self.state_len = STATE_LEN
        self.state_tensor = torch.cat([state_tensor, configuration_tensor]).float().to(self.device)
        self.state_tensor = self.state_tensor.reshape((1, 1, self.state_tensor.shape[0]))
    
    def get_cores(self):
        tensor = torch.zeros(1).to(self.device)
        if self.ActWorkerCores_2_ActWorkerRam_15616 == 1.0:
            tensor[0] = 2
            return tensor
        if self.ActWorkerCores_8_ActWorkerRam_32768 == 1.0 or self.ActWorkerCores_8_ActWorkerRam_62464 == 1.0:
            tensor[0] = 8
            return tensor
        tensor[0] = 16
        return tensor
    
    def get_ram(self):
        tensor = torch.zeros(1).to(self.device)
        if self.ActWorkerCores_2_ActWorkerRam_15616 == 1.0:
            tensor[0] = 15616
            return tensor
        if self.ActWorkerCores_8_ActWorkerRam_32768 == 1.0:
            tensor[0] = 32768
            return tensor
        
        if self.ActWorkerCores_8_ActWorkerRam_62464 == 1.0:
            tensor[0] = 62464
            return tensor
        
        if self.ActWorkerCores_16_ActWorkerRam_30720 == 1.0:
            tensor[0] = 30720
            return tensor

        tensor[0] = 65536
        return tensor
    
    def get_cardinality(self):
        tensor = torch.zeros(1).to(self.device)
        if self.ActWorkerCardinality_1 == 1.0:
            tensor[0] = 1
            return tensor
        if self.ActWorkerCardinality_2 == 1.0:
            tensor[0] = 2
            return tensor
        
        if self.ActWorkerCardinality_4 == 1.0:
            tensor[0] = 4
            return tensor
    
    def rescale(self, value, name):
        mean = torch.from_numpy(self.means_df[name].values).to(self.device)
        std = torch.from_numpy(self.stds_df[name].values).to(self.device)
        return value * std + mean


    def get_SimulationLeftNumber(self):
        return self.rescale(self.SimulationLeftNumber, 'SimulationLeftNumber')

    def get_ETPercentile(self):
        return self.rescale(self.ETPercentile, 'ETPercentile')

    def get_SimulationElapsedTime(self):
        return self.rescale(self.SimulationElapsedTime, 'SimulationElapsedTime')
    
    def get_EstimatedRemainingTime(self):
        return self.rescale(self.EstimatedRemainingTime, 'EstimatedRemainingTime')

