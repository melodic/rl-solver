# Based on rl.py and detailed-architecture.txt by DeepMind, see Suplementary Data https://www.nature.com/articles/s41586-019-1724-z#Sec2.
# By Maciej Wojtala

import torch
import numpy as np

from reinforcement_src.Trajectory import Trajectory

DISCOUNT = 0.9

epsilon = 10e-9

def loss_function(trajectories, target_logits, baselines, device,
            _lambda = 0.8, td_weight = 10.0, upgo_weight = 1.0, entropy_weight = 0.0001):
    loss = torch.sum(torch.zeros(1)).to(device)

    loss_entropy = entropy_weight * entropy_loss(target_logits)
    
    loss += loss_entropy

    loss_upgo = upgo_weight * upgo_loss(trajectories, target_logits, baselines, device)

    loss += loss_upgo

    loss_td = td_weight * td_lambda_loss(trajectories, baselines, _lambda, device)

    loss += loss_td

    return loss, loss_entropy.detach().cpu(), loss_upgo.detach().cpu(), loss_td.detach().cpu()

# Avoiding log(0), 0 >= log_logits(x) >= log(epsilon/(1.0 + epsilon))
def log_logits(logits):
    return torch.log((logits + epsilon) / (1.0 + epsilon))

def upgo_loss(trajectories, target_logits, baselines, device, _lambda = 0.8):
    advantage = upgo_value_returns(trajectories, baselines, device) - baselines[:-1]
    action_indexes = [trajectories[i].get_action_index() for i in range(len(trajectories))][:-1]
    '''rewards = torch.stack([trajectories[i].get_reward() for i in range(len(trajectories))])[:-1]
    discounts = torch.stack([1.0 - trajectories[i].get_is_final_step() for i in range(len(trajectories))])[:-1] * DISCOUNT
    action_indexes = [trajectories[i].get_action_index() for i in range(len(trajectories))][:-1]
    print("action indexes")
    print(action_indexes)


    lambdas = torch.Tensor([_lambda]).to(device)
    for _ in range(0, len(trajectories[:-1]) - 1):
        lambdas = torch.cat((lambdas, torch.Tensor([_lambda]).to(device)))
    
    lambdas = lambdas[1:]

    value_returns = td_lambda_value_returns(baselines[1:], rewards, discounts, lambdas, device)
    advantage = value_returns - baselines[:-1]'''

    assert(len(advantage) == len(target_logits[:-1]))

    return torch.mean(torch.stack([single_upgo_loss(target_logits[i].flatten(), action_indexes[i], advantage[i]) for i in range(len(target_logits[:-1]))]))

def single_upgo_loss(target_logit, action_index, single_advantage):
    detached_advantage = single_advantage.detach()
    return -1.0 * detached_advantage * log_logits(target_logit[action_index])

def upgo_value_returns(trajectories, baselines, device):
    values = baselines[:-1]
    next_values = baselines[1:]
    rewards = torch.stack([trajectories[i].get_reward() for i in range(len(trajectories))])[:-1]
    discounts = torch.stack([1.0 - trajectories[i].get_is_final_step() for i in range(len(trajectories))])[:-1] * DISCOUNT

    lambdas = torch.stack([((rewards[i] + discounts[i] * next_values[i]) >= values[i]).to(torch.float).to(device) for i in range(len(values))])
    lambdas = lambdas[1:]

    return td_lambda_value_returns(next_values, rewards, discounts, lambdas, device)


def td_lambda_loss(trajectories, baselines, _lambda, device):
    rewards = torch.stack([trajectories[i].get_reward() for i in range(len(trajectories))])[:-1]
    discounts = torch.stack([1.0 - trajectories[i].get_is_final_step() for i in range(len(trajectories))])[:-1] * DISCOUNT

    lambdas = torch.Tensor([_lambda]).to(device)
    for _ in range(0, len(trajectories[:-1]) - 1):
        lambdas = torch.cat((lambdas, torch.Tensor([_lambda]).to(device)))
    
    lambdas = lambdas[1:]

    value_returns = td_lambda_value_returns(baselines[1:], rewards, discounts, lambdas, device)
    detached_value_returns = value_returns.detach()
    
    return 0.5 * torch.mean(torch.square(detached_value_returns - baselines[:-1]))


def td_lambda_value_returns(values, rewards, discounts, lambdas, device):
    "Shape: values: L, rewards: L, discounts: L, lambdas: L-1"
    print("td")
    print("values")
    print(values)
    print("rewards")
    print(rewards)
    print("discounts")
    print(discounts)
    print("lambdas")
    print(lambdas)
    length = lambdas.shape[0]
    result = torch.zeros(length + 1).to(device)

    result[length] = rewards[length] + discounts[length] * values[length]
    
    for k in range(length - 1, -1, -1):
        result[k] = rewards[k] + discounts[k] * ((lambdas[k] * result[k + 1]) + (1 - lambdas[k]) * values[k])    

    print("result")
    print(result)
    
    return result

def entropy_loss(logits):
    return -1.0 * torch.mean(torch.stack([single_entropy_loss(logits[i]) for i in range(len(logits))]))

def single_entropy_loss(logit):
    return -1.0 * (logit*log_logits(logit)).sum() / np.log(logit.shape[-1])


