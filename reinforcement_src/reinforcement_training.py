# By Maciej Wojtala

import copy
from reinforcement_src.Agent import Agent, VALID_STEP
from reinforcement_src.get_environment_model import get_environment_model
from get_rl_models import get_rl_models
from reinforcement_src.State import STATE_LEN
from reinforcement_src.Action import ACTION_LEN
import numpy as np
import torch
import os
import pandas as pd


def main():
    print("beginning")
    device = torch.device(torch.cuda.current_device()  if torch.cuda.is_available() else 'cpu')
    print(device)
    np.random.seed(15)
    epochs = 10000000
    valid_df = pd.read_csv("./data/valid_sorts.csv")
    valid_df_len = len(valid_df)

    valid_epochs = valid_df_len * VALID_STEP
    validation_frequency = 4000

    environment_model = get_environment_model()
    environment_model.eval()

    d_t = 1
    d_f = STATE_LEN
    d_v = 1
    d_l = ACTION_LEN
    d_model = 64

    models = get_rl_models(d_t, d_f, d_v, d_l, d_model, device)

    agents = []
    for i in range(len(models)):
        agents.append(Agent(copy.deepcopy(models[i]), environment_model, device))

    for a in agents:
        a.model.eval()

    print("training")
    mean_total_losses = []
    mean_actor_losses = []
    mean_critic_losses = []
    mean_entropy_losses = []
    mean_rewards = []

    mean_valid_rewards = []

    rewards = []
    
    for i in range(epochs):
        for agent in agents:
            print("training epoch")
            print(i)
            state, action, action_index, reward, is_final = agent.make_step()
            rewards.append(reward.detach().cpu().numpy())
            print("true reward")
            print(reward.detach().cpu().numpy())
            agent.update(state, action, action_index, reward, is_final)

            if (i + 1) % validation_frequency == 0:
                mean_total_losses.append(agent.get_total_losses().mean())
                mean_total_losses_df = pd.DataFrame(mean_total_losses)
                mean_total_losses_df.to_csv('./RL_losses/mean_total_losses.csv')
                agent.set_total_losses([])
                
                mean_critic_losses.append(agent.get_td_losses().mean())
                mean_critic_losses_df = pd.DataFrame(mean_critic_losses)
                mean_critic_losses_df.to_csv('./RL_losses/mean_critic_losses.csv')
                agent.set_td_losses([])

                mean_actor_losses.append(agent.get_upgo_losses().mean())
                mean_actor_losses_df = pd.DataFrame(mean_actor_losses)
                mean_actor_losses_df.to_csv('./RL_losses/mean_actor_losses.csv')
                agent.set_upgo_losses([])

                mean_entropy_losses.append(agent.get_entropy_losses().mean())
                mean_entropy_losses_df = pd.DataFrame(mean_entropy_losses)
                mean_entropy_losses_df.to_csv('./RL_losses/mean_entropy_losses.csv')
                agent.set_entropy_losses([])

                mean_rewards.append(np.array(rewards).mean())
                mean_rewards_df = pd.DataFrame(mean_rewards)
                mean_rewards_df.to_csv('./RL_losses/mean_rewards.csv')

                rewards = []
                agent.reset_update_counter()
                agent.reset_valid_row()
                for i in range(valid_epochs):
                    _, _, _, reward, _ = agent.make_step()
                    rewards.append(reward.detach().cpu().numpy())
                    print("true reward")
                    print(reward.detach().cpu().numpy())

                    print("validation epoch")
                    print(i)
                    agent.validate()

                mean_valid_rewards.append(np.array(rewards).mean())
                mean_rewards_df = pd.DataFrame(mean_valid_rewards)
                mean_rewards_df.to_csv('./RL_losses/mean_valid_rewards.csv')
                rewards = []
                agent.reset_update_counter()
    
    return 0
