import torch
import pandas as pd
from RL_Model.Model.Model import Model

SEED = 27

VALUE_INIT = 0.1
EPOCHS = 30
BATCH_SIZE = 32

epsilon = 10e-9

def get_rl_models(d_t, d_f, d_v, d_l, d_model, device):
    torch.manual_seed(SEED)
    models = torch.nn.ModuleList([Model(d_t, d_f, d_v, d_l, d_model, device).to(device)])
    for m in models:
        for param in m.parameters():
            if param.dim() > 1:
                torch.nn.init.xavier_uniform_(param)
            else:
                torch.nn.init.normal_(param)
    
    for model in models:
        try:
            path = './Pretrained_RL_Models/RL_Model.pt'
            model_state = torch.load(path)
            model.load_state_dict(model_state)
        except:
            model = pretrain_model(model, d_v, d_l, device)
    
    return models

def pretrain_model(model, d_v, d_l, device):
    data_df = pd.read_csv("./data/train_sorts.csv").sample(frac=1).reset_index(drop=True)
    #print(data_df)
    target_baselines = torch.stack([torch.ones(d_v).to(device) * VALUE_INIT] * BATCH_SIZE).to(device)
    target_logits = torch.stack([torch.ones(d_l).to(device) * 1.0 / d_l] * BATCH_SIZE).to(device)
    print("target_baselines")
    print(target_baselines)
    print(target_baselines.shape)
    print("target_logits")
    print(target_logits)
    print(target_logits.shape)

    state_index = [
            'SimulationLeftNumber' == data_df.columns[i]
            or 'SimulationElapsedTime' == data_df.columns[i]
            or 'ETPercentile' == data_df.columns[i]
            or 'WillFinishTooSoon' == data_df.columns[i]
            or 'EstimatedRemainingTime' == data_df.columns[i]
            or 'NotFinished' == data_df.columns[i]
            or 'NotFinishedOnTime' == data_df.columns[i]
            or 'NotFinishedOnTimeContext' == data_df.columns[i]
            or 'MinimumCores' == data_df.columns[i]
            for i in range(len(data_df.columns))
    ]

    action_index = [
            'ActWorkerCardinality_1' == data_df.columns[i]
            or 'ActWorkerCardinality_2' == data_df.columns[i]
            or 'ActWorkerCardinality_4' == data_df.columns[i]
            or 'ActWorkerCardinality_5' == data_df.columns[i]
            or 'ActWorkerCardinality_6' == data_df.columns[i]
            or 'ActWorkerCores_2_ActWorkerRam_15616' == data_df.columns[i]
            or 'ActWorkerCores_8_ActWorkerRam_32768' == data_df.columns[i]
            or 'ActWorkerCores_8_ActWorkerRam_62464' == data_df.columns[i]
            or 'ActWorkerCores_16_ActWorkerRam_30720' == data_df.columns[i]
            for i in range(len(data_df.columns))
    ]

    env_state_df = data_df.iloc[:, state_index]
    action_df = data_df.iloc[:, action_index]
    env_state_tensor = torch.from_numpy(env_state_df.values).to(device)
    action_tensor = torch.from_numpy(action_df.values).to(device)
    print(env_state_tensor)
    print(action_tensor)
    inputs = torch.cat([env_state_tensor, action_tensor], dim = -1).float().to(device)
    print(inputs)
    print(len(inputs))
    optim = torch.optim.Adam(model.parameters(), lr=0.0003)
    model.train()

    for epoch in range(EPOCHS):
        print("Epoch: " + str(epoch))
        for batch in range(0, len(inputs), BATCH_SIZE):
            optim.zero_grad()
            input = inputs[batch : batch + BATCH_SIZE]
            input = input.reshape((input.shape[0], 1, input.shape[1]))

            logits, baselines = model(input)
            if batch % 31 == 0:
                print("logits")
                print(logits)
                print("baselines")
                print(baselines)

                loss = mse(baselines, target_baselines[:len(baselines)]) + kl_div(logits, target_logits[:len(logits)])
            
                loss.backward()
                optim.step()
        torch.save(model.state_dict(), './Pretrained_RL_Models/RL_Model.pt')


    return model

def mse(p, q):
    return torch.mean(torch.square(p - q))

def kl_div(p_logits, q_logits):
    log_p_logits = log_logits(p_logits)
    log_q_logits = log_logits(q_logits)
    #masks = logit.detach().clone() # masks are logits distribution

    #return torch.sum(masks * supervised_logit * (log_supervised_logit - log_logit))
    return torch.mean(p_logits * (log_p_logits - log_q_logits))

def log_logits(logits):
    return torch.log((logits + epsilon) / (1.0 + epsilon))